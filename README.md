# Ignis Tube

Video streaming service/project

## Video Player

[Plyr](https://github.com/sampotts/plyr)

## Difference from competition

- Download button if content creators allow it
- Free subscriptions
- Paid membership
  - option for content creators to give videos to members only
  - support content creators
- Podcasts
- Tending page is actually trending videos
  - Page w/ top subbed and top viewed views of time period (today, week, month, year, all time)

## Timeline/tasks

[Trello](https://trello.com/b/L0Cg3Psf/tasks)
